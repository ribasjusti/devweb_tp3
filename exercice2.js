var idCourrant = 0;

var tab = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

tab[0] = "The Imitation Game\nRésumé : 1940 : Alan Turing, mathématicien, cryptologue, est chargé par le gouvernement Britannique de percer le secret de la célèbre machine de cryptage allemande Enigma, réputée inviolable.";
tab[1] = "The Kings Man : Première mission\nRésumé : Lorsque les pires tyrans et génies criminels de l’Histoire se réunissent pour planifier l’élimination de millions d’innocents, un homme se lance dans une course contre la montre pour contrecarrer leurs plans. Découvrez les origines de la toute première agence de renseignement indépendante.";
tab[2] = "La ligne verte\nRésumé : Paul Edgecomb, pensionnaire centenaire d'une maison de retraite, est hanté par ses souvenirs. Gardien-chef du pénitencier de Cold Mountain en 1935, il était chargé de veiller au bon déroulement des exécutions capitales en s’efforçant d'adoucir les derniers moments des condamnés. Parmi eux se trouvait un colosse du nom de John Coffey, accusé du viol et du meurtre de deux fillettes. Intrigué par cet homme candide et timide aux dons magiques, Edgecomb va tisser avec lui des liens très forts.";
tab[3] = "Miss Périgrine et les enfants particuliers\n Résumé : À la mort de son grand-père, Jacob découvre les indices et l’existence d’un monde mystérieux qui le mène dans un lieu magique : la Maison de Miss Peregrine pour Enfants Particuliers. Mais le mystère et le danger s’amplifient quand il apprend à connaître les résidents, leurs étranges pouvoirs …  et leurs puissants ennemis. Finalement, Jacob découvre que seule sa propre 'particularité' peut sauver ses nouveaux amis.";
tab[4] = "OSS 117 : Alerte rouge en Afrique Noire\n Résumé : 1981. Hubert Bonisseur de La Bath, alias OSS 117, est de retour. Pour cette nouvelle mission, plus délicate, plus périlleuse et plus torride que jamais, il est contraint de faire équipe avec un jeune collègue, le prometteur OSS 1001.";
tab[5] = "Riverdale\n Résumé : Sous ses airs de petite ville tranquille, Riverdale cache en réalité de sombres secrets. Alors qu'une nouvelle année scolaire débute, le jeune Archie Andrews et ses amis Betty, Jughead, et Kevin voient leur quotidien bouleversé par la mort mystérieuse de Jason Blossom, un de leurs camarades de lycée. Alors que les secrets des uns et des autres menacent de remonter à la surface, et que la belle Veronica, fraîchement débarquée de New York, fait une arrivée remarquée en ville, plus rien ne sera jamais comme avant à Riverdale...";
tab[6] = "Soul\n Résumé : Passionné de jazz et professeur de musique dans un collège, Joe Gardner a enfin l’opportunité de réaliser son rêve : jouer dans le meilleur club de jazz de New York. Mais un malencontreux faux pas le précipite dans le « Grand Avant » – un endroit fantastique où les nouvelles âmes acquièrent leur personnalité, leur caractère et leur spécificité avant d’être envoyées sur Terre. Bien décidé à retrouver sa vie, Joe fait équipe avec 22, une âme espiègle et pleine d’esprit, qui n’a jamais saisi l’intérêt de vivre une vie humaine. En essayant désespérément de montrer à 22 à quel point l’existence est formidable, Joe pourrait bien découvrir les réponses aux questions les plus importantes sur le sens de la vie.";
tab[7] = "Stranger Things\n Résumé : A Hawkins, en 1983 dans l'Indiana. Lorsque Will Byers disparaît de son domicile, ses amis se lancent dans une recherche semée d’embûches pour le retrouver. Dans leur quête de réponses, les garçons rencontrent une étrange jeune fille en fuite. Les garçons se lient d'amitié avec la demoiselle tatouée du chiffre '11' sur son poignet et au crâne rasé et découvrent petit à petit les détails sur son inquiétante situation. Elle est peut-être la clé de tous les mystères qui se cachent dans cette petite ville en apparence tranquille…";
tab[8] = "The holidays\n Résumé : Une Américaine (Amanda) et une Anglaise (Iris), toutes deux déçues des hommes, décident, sans se connaître, d'échanger leurs appartements. Iris, va débarquer dans une demeure de rêve tandis que la distinguée Amanda découvre une petite maison de campagne sans prétentions. Les deux femmes pensent passer de paisibles vacances loin de la gent masculine, mais c'était sans compter l'arrivée du frère d'Iris dans la vie d'Amanda, et la rencontre de Miles pour Iris.";
tab[9] = "Les Tuches 4\n Résumé : Trois ans après le succès du dernier opus et un tournage suspendu, Jeff Tuche et sa tribu reviennent dans un quatrième film annoncé au 9 décembre 2020 et se dévoile à travers un premier poster.";

/* Fonction qui fait apparaître un pop up lorsqu'on clique sur le boutton */
function boutton() {
  var image = document.getElementById(idCourrant);
  var source = image.getAttribute("src");
  var nom = image.getAttribute("alt");
  var largeur = image.width;
  var hauteur = image.height;
  alert("Source : " + source + "\nNom : " + nom + "\nTaille : " + largeur + " x " + hauteur);
}

/* Fonction qui fait apparaître un pop up lorsqu'on clique sur le boutton */
function film() {
  alert(tab[idCourrant]);
}

/* Fonction qui change l'image lorsque l'on passe la sourie dessus */
function img1() {
  var image = document.getElementById(idCourrant);
  image.setAttribute("src", "./images/kitty.jpeg");
}

/* Fonction qui change l'image lorsque l'on passe la sourie dessus */
function img2() {
  var image = document.getElementById(idCourrant);
  image.setAttribute("src", "./images/img" + idCourrant + ".jpg");
}

/* Fonction qui tire un entier aléatoirement entre 0 et max */
function getRandomInt(max) {
  return Math.floor(Math.random() * Math.floor(max));
}

/* Fonction qui tire un nombre aléatoire pour choisir une image */
function changeIMG() {
  var image = document.getElementById(idCourrant);
  idCourrant = getRandomInt(10);
  image.setAttribute("src", "./images/img" + idCourrant +".jpg");
  image.setAttribute("id", idCourrant);
  image.setAttribute("alt", "img" + idCourrant);
}
