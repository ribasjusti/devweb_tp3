var tour = 0;

var tab = [0,0,0,0,0,0,0,0,0]

n = prompt("Pour jouer avec le mode Joueur VS Ordi, saisissez '0', sinon saisissez '1'");

/* Fonction qui simule un coup */
function coup(image) {
  if (aGagne() == 0) {
    if (n == 0) {
      jouerJO(image);
    } else {
      jouerJJ(image);
    }
    if (aGagne() != 0) {
      alert("Victoire du Joueur " + aGagne());
    } else {
      if (tour >= 9) {
        alert("Personne n'a gagné...");
      }
    }
  }
}

/* Fonction qui tire un entier aléatoirement entre 0 et max */
function getRandomInt(max) {
  return Math.floor(Math.random() * Math.floor(max));
}

/*Fonction qui permet de jouer en Joueur VS Joueur */
function jouerJJ(image) {
  // On vérifie que la case est bien vide
  if (image.getAttribute("src") == "./img/fond.png") {
    // On regarde la parité du nombre de tour pour que les joueurs jouent à tour de rôle
    if (tour%2 == 0) {
      image.setAttribute("src", "./img/fond1.png");
      tab[image.getAttribute("id")] = 1;
    } else {
      image.setAttribute("src", "./img/fond2.png");
      tab[image.getAttribute("id")] = 2;
    }
    tour++;
  }
}

/*Fonction qui permet de jouer en Joueur VS Ordi */
function jouerJO(image) {
  // On vérifie que la case est bien vide
  if (image.getAttribute("src") == "./img/fond.png") {
    // Le joueur commence
    image.setAttribute("src", "./img/fond1.png");
    tab[image.getAttribute("id")] = 1;
    tour++;
    // L'ordi joue ensuite
    if (tour < 9) {
      do {
        var image2 = document.getElementById(getRandomInt(9));
      } while (image2.getAttribute("src") != "./img/fond.png")
      image2.setAttribute("src", "./img/fond2.png");
      tab[image2.getAttribute("id")] = 2;
    }
    tour++;
  }
}



/* Fonction qui vérifie si un joueur a gagné */
function aGagne() {
  var res = 0;
  for(i=0; i<3; i++){
    //On vérifie pour les lignes
    if((tab[i*3] == tab[i*3+1]) && (tab[i*3+1] == tab[i*3+2]) && (tab[i*3] != 0)) {
      res = tab[i*3];
    }
    //On vérifie pour les colonnes
    if ((tab[i] == tab[i+3]) && (tab[i+3] == tab[i+6]) && (tab[i] != 0)) {
      res = tab[i];
    }
  }
  //On vérifie pour les deux colonnes
  if ((tab[0] == tab[4]) && (tab[4] == tab[8]) && (tab[0] != 0)) {
    res = tab[0];
  }
  if ((tab[2] == tab[4]) && (tab[4] == tab[6]) && (tab[2] != 0)) {
    res = tab[2];
  }
  return(res);
}

/* Fonction qui permet de recommencer une partie */
function recommencer() {
  location.reload();
  // On réinitialise le tableau de valeurs
  tab = [0,0,0,0,0,0,0,0,0]
  // On réinitialise le nombre de tours
  tour = 0;
}
