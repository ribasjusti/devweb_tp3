/* Fonction pour calculer le terme de la suite de Fibonacci */
function calcul(n) {
  if(n >= 2){
    var u = 0;
    var v = 1;
    for(i=1; i<n; i++){
      var w = u + v;
      u = v;
      v = w;
      console.log("u : ", u);
      console.log("v : ", v);
    }
    var terme = v;
  } else {
    var terme = n;
  }
  return(terme);
}

/* L'utilisateur saisie le rang de la suite */
n = prompt("Quel est le rang de la suite?");
console.log("n : ", n);

/* On récupère l'heure d'exécution du script */
d = new Date();
h = d.getHours();
m = d.getMinutes();

/* On affiche dans la console */
console.log("h : ", h);
console.log("m : ", m);

/* Ici, on rajoute dynamiquement une div */
document.write("<div>");
document.write("A ", h, "h", m, "<br/>Le ", n, "<sup>e</sup> terme de la suite de Fibonnacci est ", calcul(n));
document.write("</div'>");
